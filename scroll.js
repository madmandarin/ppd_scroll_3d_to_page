mandarinScroll = function(id) {
/* ---------------------------- begin view ----------------------------- */
	'use strict';

	var view = {

		setElementHide: function(pr) {
			var el = document.getElementById(id);
			el.style.transform = 'scale('+Math.sqrt(pr, 16)+')';
			el.style.opacity =  Math.pow(pr, 1);
		},
		setElementAutoScroll: function(route, height, pos, time) {
			var scrollDirection;
			var way;
			if (route == 'down') {
				way = (height - pos);
				scrollDirection = function(progress) {
					var temp = Math.ceil(way * progress);
					temp = temp + pos;
					scrollTo(0, temp);
				}
			} else {
				way = window.pageYOffset || document.documentElement.scrollTop;
				scrollDirection = function(progress) {
					var temp = Math.ceil(way * progress);
					temp = way - temp;
					scrollTo(0, temp);
				}
				
			}
			this.animate({
				duration: (time),
				draw: scrollDirection,
				timing: function (timeFraction) {
					if (timeFraction < 0) {
						return 0;
					} 
	  				return Bezier.cubicBezier(0.180, 0.605, 0.330, 1, timeFraction);
				},
			});
		},
		animate: function (options) {
		  var start = performance.now();

		  requestAnimationFrame(function animate(time) {
		    // timeFraction от 0 до 1
		    var timeFraction = (time - start) / options.duration;
		    if (timeFraction > 1) timeFraction = 1;

		    // текущее состояние анимации
		    var progress = options.timing(timeFraction)

		    options.draw(progress);

		    if (timeFraction < 1) {
		      requestAnimationFrame(animate);
		    }

		  });
		}
	};


/* ----------------------------- end view ------------------------------ */

	

/* ---------------------------- begin model ---------------------------- */
	
	var model = {
		oldScrollPosition: 0,
		scrollYpos: window.pageYOffset || document.documentElement.scrollTop,

		route: function() {
			this.scrollYpos = window.pageYOffset || document.documentElement.scrollTop;
			var route = this.oldScrollPosition - this.scrollYpos;
			this.oldScrollPosition = this.scrollYpos;
			if (route > 0) {
				return 'up';
			}
			if (route < 0) {
				return 'down';
			}
		},
		posAutoScroll: function() {
			var height = document.documentElement.clientHeight;
			return height * 0.20;
		},
		calcYpercent: function() {
			var height = document.documentElement.clientHeight;
			var percent = 1 - this.scrollYpos / height;
			return percent;
		}
	};

/* ----------------------------- end model ----------------------------- */


/* -------------------------- begin controller ------------------------- */
	
	var controller = {
		scrolling: false,
		autoBack: false,
		backScroll: 0,
		backScrollFixing: 0,

		scroll: function() {
			
			var timeScroll = 800;
			view.setElementHide( model.calcYpercent() );
			
			if ( (window.pageYOffset || document.documentElement.scrollTop) < document.documentElement.clientHeight) {
				if ( model.route() == 'down' ) {
					if ( !(controller.autoBack) && !(controller.scrolling) ) {
						controller.autoBack = true;
						controller.backScroll = setTimeout(function() {
							controller.scrolling = true;

							view.setElementAutoScroll('up', document.documentElement.clientHeight, model.scrollYpos, (timeScroll/3) );

							controller.backScrollFixing = setTimeout(function() {
								controller.scrolling = false;
								controller.autoBack = false;
								scrollTo(0, 0);
								view.setElementHide( 1 );
							}, (timeScroll/3) );
						}, 400);
					};
					if ( !(controller.scrolling) ) {
						if ( (model.scrollYpos > model.posAutoScroll()) && 
							 (model.scrollYpos < document.documentElement.clientHeight) ) {
							controller.scrolling = true;
							clearTimeout(controller.backScroll);
							clearTimeout(controller.backScrollFixing);

							view.setElementAutoScroll('down', document.documentElement.clientHeight, model.scrollYpos, timeScroll);
							
							setTimeout(function() {
								controller.scrolling = false;
								controller.autoBack = false;
								scrollTo(0, document.documentElement.clientHeight);
								view.setElementHide( 0 );
							}, timeScroll);
						}
					}
				} else {
					if ( !(controller.autoBack) && !(controller.scrolling) ) {
						controller.autoBack = true;
						controller.backScroll = setTimeout(function() {
							controller.scrolling = true;

							view.setElementAutoScroll('down', document.documentElement.clientHeight, model.scrollYpos, (timeScroll/3) );

							controller.backScrollFixing = setTimeout(function() {
								controller.scrolling = false;
								controller.autoBack = false;
								scrollTo(0, document.documentElement.clientHeight);
								view.setElementHide( 0 );
							}, (timeScroll/3) );
						}, 400);
					};
					if ( !(controller.scrolling) ) {		
						if ( ( model.scrollYpos ) < 
							 ( document.documentElement.clientHeight - model.posAutoScroll() ) ) {

							controller.scrolling = true;
							clearTimeout(controller.backScroll);
							clearTimeout(controller.backScrollFixing);

							view.setElementAutoScroll('up', document.documentElement.clientHeight, model.scrollYpos, timeScroll);
							
							setTimeout(function() {
								controller.scrolling = false;
								controller.autoBack = false;
								scrollTo(0, 0);
								view.setElementHide( 1 );
							}, timeScroll);
						}
					}
				}
			}
		}
	};

/* --------------------------- end controller -------------------------- */




/* --------------------- anonymous initialize function ----------------- */
	(function() {

		var app = {

			init: function () {
				this.main();
				this.event();
			},

			main: function () {

			},

			event: function () {

				window.onscroll = controller.scroll;
			}

		};
		
		app.init();

	}());
/* --------------------- anonymous initialize function ----------------- */
};